// Copyright 2022 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

pub use std::cell::{RefCell, RefMut};
pub use std::cmp::{self, Ordering};
pub use std::collections::{HashMap, HashSet, BTreeMap, BTreeSet};
pub use std::convert::{TryFrom, TryInto};
pub use std::env;
pub use std::error::Error as StdError;
pub use std::fmt::{self, Debug, Display};
pub use std::fs::{self, File};
pub use std::io::{self, BufRead, BufReader, BufWriter, Write as _};
pub use std::iter;
pub use std::mem;
pub use std::ops;
pub use std::process::exit;
pub use std::rc::Rc;
pub use std::str::FromStr;
pub use std::thread::panicking;

pub use anyhow::anyhow;
pub use anyhow::Ok as AOk;
pub use anyhow::Context as _;
pub use paste::paste;

pub(crate) use crate::data::*;
pub(crate) use crate::file::*;
pub(crate) use crate::logs::*;
pub(crate) use crate::numbers::*;

pub type AE = anyhow::Error;
pub type AR<T> = Result<T,AE>;
