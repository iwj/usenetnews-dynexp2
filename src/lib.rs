// Copyright 2022 Ian Jackson
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

#![doc = include_str!("../README.md")]

//#![allow(dead_code,unused_variables,unused_mut)]

#![allow(clippy::nonminimal_bool)]
#![allow(clippy::let_and_return)]

pub mod prelude;

mod file;
mod data;
mod calc;
mod numbers;
mod logs;

#[cfg(test)]
mod test;

pub use data::Loaded;
pub use logs::LogCollection;
