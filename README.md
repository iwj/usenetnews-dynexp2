### USENET news server expiry time (INN2 `expire.ctl`) dynamic tuning

This is a program for automatically tuning the expiry times
in `expire.ctl`, according to the available disk space.

It has been deployed on chiark.greenend.org.uk since December 2022,
but it is not currently documented.

### Version history

 * 0.1.2: Initial release after initial deployment and tests
 * 0.1.1: Version number burned due to trouble involving cargo after git tag
 * 0.1.0: Version number burned due to trouble involving cargo after git tag
